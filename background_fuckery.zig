const std = @import("std");
const mem = std.mem;
const posix = std.posix;
const rand = std.rand;
const heap = std.heap;
const fmt = std.fmt;
const time = std.time;

const wayland = @import("wayland");
const common = @import("common");

const wl = wayland.client.wl;
const zwlr = wayland.client.zwlr;
const xdg = wayland.client.xdg;
const layer_shell = zwlr.common.zwlr.layer_shell_v1;

const stdout = std.io.getStdOut().writer();
const stdin = std.io.getStdIn();

const Context = struct {
    shm: ?*wl.Shm,
    compositor: ?*wl.Compositor,
    shell: ?*zwlr.LayerShellV1,

    surface: *wl.Surface,

    layer_surface: *zwlr.LayerSurfaceV1,
    wm_base: *xdg.WmBase,

    width: u32,
    height: u32,
    cellwidth: u32,
    running: bool,

    buffer: ?*wl.Buffer,
    block: []u32,
};

const Board = struct {
   board : []u8,
   buffer : []u8,
   width : u32,
   height : u32,
};

const GameType = struct{
    states : u8,
    colors : []const u32,
    update : *const fn(*Game) void,
    initialize   : *const fn(*Game) void,
    framerate : u64,

    pub fn init(gametype: GameType, gamewidth: u32, gameheight: u32, allocator: mem.Allocator) !Game
    {
        var game = Game {
            .board = Board {
                .width = gamewidth,
                .height = gameheight,
                .board = try allocator.alloc(u8, gamewidth * gameheight) ,
                .buffer = try allocator.alloc(u8, gamewidth * gameheight),
            },
            .gameType = gametype,
        };

        gametype.initialize(&game);

        return game;
    }
};

const Game = struct {
    board : Board,
    gameType : GameType,

    pub fn render(self: *Game, context: *Context) !void
    {
        const board = self.board;
        const gameType = self.gameType;
        const cellwidth = context.cellwidth;

        for (0..context.width) |x|
        {
            for (0..context.height) |y|
            {
                const index = (x / cellwidth) + (y / cellwidth) * board.width;
                const state = board.board[index];
                // try stdout.print("x: {d} y: {d} index: {d}, val: {d}\n", .{x, y, index, state});
                const colour = gameType.colors[state];
                context.block[x + y * context.width] = colour;
            }
       }
    }

    pub fn update(self: *Game) void
    {
        self.gameType.update(self);
    }
};


const Conway = GameType{
    .states = 2,
    .colors = &[2]u32 { RGB(20, 20, 20),
                        RGB(0, 120, 0)},
    .update = conway,
    .initialize = random,
    .framerate = 16,
};

const Sandpile = GameType{
    .states = 4,
    .colors = &[4]u32 { RGB(34, 34, 34),
                        RGB(0, 77, 51),
                        RGB(0, 130, 0),
                        RGB(113, 170, 0)},
    .update = sandpile,
    .initialize = empty,
    .framerate = 1000,
};

const MultiSandpile = GameType{
    .states = 4,
    .colors = &[4]u32 { RGB(34, 34, 34),
                        RGB(0, 77, 51),
                        RGB(0, 130, 0),
    RGB(113, 170, 0)},
    .update = fast_sandpile ,
    .initialize = empty,
    .framerate = 1000,
};

const Spread = GameType{
    .states = 2,
    .colors = &[2]u32 { RGB(20, 20, 20),
                        RGB(0, 120, 0)},
    .update = spread,
    .initialize = center,
    .framerate = 10,
};

const Cyclic = GameType{
    .states = 20,
    .colors = colorgradient(20),
    .update = cyclic,
    .initialize = random,
    .framerate = 30,
};

const Pattern = GameType{
    .states = 2,
    .colors = &[2]u32 { RGB(20, 20, 20),
                        RGB(0, 120, 0)},
    .update = pattern,
    .initialize = random_dots(5),
    .framerate = 10,
};

const Brain = GameType{
    .states = 3,
    .colors = &[3]u32 { RGB(20, 20, 20),
                        RGB(0,77, 0),
                        RGB(0, 120, 0)},
    .update = brain,
    .initialize = random,
    .framerate = 16,
};

const Cyclic_Orth = GameType{
    .states = 12,
    .colors = colorgradient(12),
    .update = cyclic_orth,
    .initialize = random,
    .framerate = 30,
};

fn parseint(prompt: [] const u8) !u32
{
    var input: [20]u8 = undefined;

    try stdout.print("{s}", .{prompt});
    const count = try stdin.read(&input);

    return try fmt.parseInt(u32, mem.trim(u8, input[0..count], "\r\n"), 10);
}


pub fn main() !void
{
    var arena = heap.ArenaAllocator.init(heap.page_allocator);
    defer arena.deinit();

    const allocator = arena.allocator();

    const display = try wl.Display.connect(null);
    defer display.disconnect();
    const registry = try display.getRegistry();
    defer registry.destroy();

    var context = Context{
        .shm = null,
        .compositor = null,
        .shell = null,

        .surface = undefined,

        .layer_surface = undefined,
        .wm_base = undefined,

        .width = 0,
        .height = 0,
        .cellwidth = 2,
        .running = true,
        
        .block = undefined,
        .buffer = null,
    };


    context.cellwidth = parseint("cellwidth: ") catch 2;

    var gametype : GameType = switch(parseint("gametype: ") catch 0)
    {
        0 => Conway,
        1 => Sandpile,
        2 => Spread,
        3 => Cyclic,
        4 => Pattern,
        5 => Brain,
        6 => Cyclic_Orth,
        7 => MultiSandpile,

        else => Conway,
    };
    

    registry.setListener(*Context, registryListener, &context);
    if (display.roundtrip() != .SUCCESS) return error.RoundtripFailed;


    context.surface = context.compositor.?.createSurface() catch return;
    defer context.surface.destroy();

    context.layer_surface = context.shell.?.getLayerSurface(context.surface, null, .background , "background_fuckery") catch return;
    defer context.layer_surface.destroy();

    context.layer_surface.setExclusiveZone(-1);
    context.layer_surface.setAnchor(.{.top=true, .bottom=true, .left=true, .right=true});
    context.layer_surface.setListener(*Context, layerSurfaceListener, &context);

    context.surface.commit();
    if (display.roundtrip() != .SUCCESS) return error.RoundtripFailed;

    const gamewidth = context.width / context.cellwidth +
        @intFromBool(context.width % context.cellwidth != 0);
    const gameheight = context.height / context.cellwidth +
        @intFromBool(context.height % context.cellwidth != 0);

    try stdout.print("game resolution: {}x{}\n", .{gamewidth, gameheight});
        
    var game = try gametype.init(gamewidth, gameheight, allocator);

    // try stdout.print("game: w: {d}, h: {d}\n", .{game.board.width, game.board.height});

    var last_time = time.microTimestamp();

    const microsPerFrame = 1000000 / gametype.framerate;

    var frametimes : [10]u64 = [_]u64{0} ** 10;

    try stdout.print("maximum updates per second:\n", .{});
    
    while (context.running) {

        game.update();
        try game.render(&context);

        context.surface.attach(context.buffer, 0, 0);
        context.surface.damageBuffer(0, 0, @intCast(context.width), @intCast(context.height));
        context.surface.commit();

        _ = display.flush();
        if (display.dispatchPending() != .SUCCESS) return error.DispatchFailed;

        //try stdout.print("dispatch failed\n", .{});

        const now = time.microTimestamp();
        const diff :u64 = @intCast(now - last_time);
        var avg : f64 = @floatFromInt(diff);
        for (0..frametimes.len - 1) |i|
        {
           frametimes[i] = frametimes[i + 1];
           avg += @floatFromInt(frametimes[i + 1]);
        }
        frametimes[9] = diff;

        try stdout.print("\r\x1b[J", .{});
        try stdout.print("{d}", .{1000000 / (avg / frametimes.len)});

        if (now - last_time < microsPerFrame)
            time.sleep(1000 * (microsPerFrame - diff));

        last_time = now;
    }
    
}

////////////////////////////////////////////////////////////////////////////////////////

fn conway(game: *Game) void
{
    var neighbors : i9 = 0;
    var board = game.board;
    const width = board.width;
    const height = board.height;

    for (0..width) |x|
    {
        for (0..height) |y|
    	{
        	const index = x + width * y; 
    		// get the number of neighbors
    		neighbors = 0;

    		for (0..3) |i|
    		{
        		for (0..3) |j|
    			{
    				neighbors += board.board[((x + i + width - 1) % width) +
    				                        width * ((y + j + height - 1) % height)];
    			}
    		}

    		neighbors -= board.board[index];

    		// see whether the cell survives
    		if (neighbors < 2 or neighbors > 3)
    		{
    			board.buffer[index] = 0;
    		}
    		else if (neighbors == 3)
    		{
    			board.buffer[index]  = 1;
    		}
    		else
    		{
    			board.buffer[index]  = board.board[index];
    		}

    	}
    }

    @memcpy(board.board, board.buffer);
    // const tmp = board.board.ptr;
    // game.board.board.ptr = board.buffer.ptr;
    // game.board.buffer.ptr = tmp;
}

fn multipile(game: *Game, amount: u64, posx: u32, posy: u32) void {
    const width = game.board.width;
    const height = game.board.height;
    const index = posx + width * posy;
    var board = game.board.board;

	if (amount <= 0 or posy > height or posx > width or posx < 0 or posy < 0) return;

    const sand: u64 = board[index] + amount;
    board[index] = @intCast(sand % 4);

	if (sand  < 4) return;

	multipile(game, sand / 4, posx, (posy + 1) % height);
	multipile(game, sand / 4, (posx + 1) % width, posy);
	multipile(game, sand / 4, (posx - 1) % width, posy);
	multipile(game, sand / 4, posx, (posy - 1) % height);

	return;
}

inline fn pile(game: *Game, posx: u32, posy: u32) void {
    multipile(game, 1, posx, posy);
}

fn sandpile(game: *Game) void {
	pile(game, (game.board.width) / 2 - 2, (game.board.height) / 2);
}

fn fast_sandpile(game: *Game) void {
    multipile(game, 1 << 12 , game.board.width / 2, game.board.height / 2);
}

fn spread(game: *Game) void
{
    var board = game.board;
    const width = board.width;
    const height = board.height;

    for (0..width) |x|
    {
        for (0..height) |y|
    	{
        	const index = x + width * y; 
    		// get the number of neighbors
    		var neighbors: i32 = 0;

    		for (0..3) |i|
    		{
				neighbors += board.board[((x + i + width - 1) % width) + width * y];
				neighbors += board.board[x + width * ((y + i +  height - 1) % height)];
    		}

    		neighbors -= 2 * board.board[index];

    		// see whether the cell survives
    		if (neighbors == 1 or neighbors == 3)
    		{
    			board.buffer[index] = 1;
    		}
    		else
    		{
        		board.buffer[index] = board.board[index];
    		}


    	}
    }

    @memcpy(board.board, board.buffer);
}

fn pattern(game: *Game) void
{
    var board = game.board;
    const width = board.width;
    const height = board.height;

    for (0..width) |x|
    {
        for (0..height) |y|
    	{
        	const index = x + width * y; 
    		// get the number of neighbors
    		var neighbors: i32 = 0;

    		for (0..3) |i|
    		{
				neighbors += board.board[((x + i + width - 1) % width) + width * y];
				neighbors += board.board[x + width * ((y + i +  height - 1) % height)];
    		}

    		neighbors -= 2 * board.board[index];

    		// see whether the cell survives
			board.buffer[index] = @intFromBool(neighbors == 1 or neighbors == 3);

    	}
    }

    @memcpy(board.board, board.buffer);
}

fn cyclic(game: *Game) void
{
    var board = game.board;
    const width = board.width;
    const height = board.height;

	for (0..width) |r|
	{
		for (0..height) |c|
		{
			const cur = board.board[r + width * c];
			const target = (cur + 1) % game.gameType.states;
			var next = cur;

			// Check the surrounding 8 cells
			for (0..3) |dr|
			{
				for (0..3) |dc|
				{
					if (dr == 0 and  dc == 0)
					    continue;

					if (board.board[ ((r + dr + width - 1) % width) + width * ((c + dc + height - 1) % height) ] == target)
						next = target;
					
				}
			}

			board.buffer[r + width * c] = next;
		}
	}

	// replace current state with future state as the fututre is NOW
    @memcpy(board.board, board.buffer);
}

fn brain(game: *Game) void
{
    var board = game.board;
    const width = board.width;
    const height = board.height;

    for (0..width) |x|
    {
        for (0..height) |y|
    	{
        	const index = x + width * y; 
    		// get the number of neighbors
    		var neighbors: i32 = 0;

    		for (0..3) |i|
    		{
        		for (0..3) |j|
    			{
    				neighbors += @intFromBool(board.board[((x + i + width - 1) % width) +
    				                        width * ((y + j + height - 1) % height)] == 2);
    			}
    		}

    		neighbors -= @intFromBool(board.board[index] == 2);


            if (neighbors == 2 and board.board[index] == 0)
            {
                board.buffer[index] = 2;
            }
            else if (board.board[index] > 0)
            {
                board.buffer[index] = board.board[index] - 1;
            }
            else
            {
                board.buffer[index] = 0;
            }
    	}
    }

    @memcpy(board.board, board.buffer);
}

fn cyclic_orth(game: *Game) void
{
    var board = game.board;
    const width = board.width;
    const height = board.height;

	for (0..width) |r|
	{
		for (0..height) |c|
		{
			const cur = board.board[r + width * c];
			const target = (cur + 1) % game.gameType.states;
			var next = cur;

			// Check the surrounding 8 cells
			for (0..3) |d|
			{
				if (d == 1)
				    continue;

				if (board.board[ ((r + d + width - 1) % width) + width * c] == target)
					next = target;

				if (board.board[r + width * ((c + d + height - 1) % height) ] == target)
					next = target;
			}

			board.buffer[r + width * c] = next;
		}
	}

	// replace current state with future state as the fututre is NOW
    @memcpy(board.board, board.buffer);
}
//////////////////////////////////////////////////////////////////////////////////////////

fn random(game: *Game) void
{
    var board = game.board;
    const states = game.gameType.states;
    var isaac = rand.Isaac64.init(@bitCast(std.time.timestamp()));
    var gen = isaac.random();
    for (0..board.width * board.height) |i|
    {
        board.board[i] = gen.int(u8) % states;
    }
}

fn empty(game: *Game) void
{
    @memset(game.board.board, 0);
}

fn center(game: *Game) void
{
    @memset(game.board.board, 0);
    const board = game.board;
    game.board.board[(board.width + board.width * board.height) / 2] = 1;
}

fn random_dots(comptime num: u64) fn(*Game) void
{
    return struct {
        pub fn f (game: *Game) void {
            @memset(game.board.board, 0);
            const board = game.board;
            var isaac = rand.Isaac64.init(@bitCast(std.time.timestamp()));
            var gen = isaac.random();

            for (0..num) |_| {
                const w = gen.intRangeAtMost(u32, 0, board.width);
                const h = gen.intRangeAtMost(u32, 0, board.height);
                game.board.board[w + board.width * h] = 1;
            }
        }
    }.f;
}

//////////////////////////////////////////////////////////////////////////////////////////

fn colorgradient(comptime steps: u32) []const u32
{
   comptime var palette: [steps]u32 = undefined;
   for (0..steps) |i|
   {
       palette[i] = HlsToRgb(360 * i / steps, 0.5, 0.5);
   }

   const out = palette;

   return out[0..steps];
}

fn RGB(comptime R : u8, comptime G : u8, comptime B : u8) u32
{
    const bytes : [4]u8 = [4]u8{255, R, G, B};

	return mem.bigToNative(u32,mem.bytesToValue(u32, &bytes));

}

fn QqhToRgb(q1: f64, q2: f64, hue: f64) f64
{
    var mhue = hue;
	if (mhue > 360)
	{
    	mhue -= 360;
	}
	else if (mhue < 0)
	{
    	mhue += 360;
    }

	if (mhue < 60)
	    return q1 + (q2 - q1) * mhue / 60;
	if (mhue < 180)
	    return q2;
	if (mhue < 240)
	    return q1 + (q2 - q1) * (240 - mhue) / 60;
	return q1;
}

fn HlsToRgb(h: f64, l: f64, s: f64) u32
{
	var p2: f64 = undefined;
	if (l <= 0.5)
	{
    	p2 = l * (1 + s);
	}
	else
	{
    	p2 = l + s - l * s;
	}

	const p1: f64 = 2 * l - p2;
	var f64_r: f64 = undefined;
	var f64_g: f64 = undefined;
	var f64_b: f64 = undefined;
	if (s == 0)
	{
		f64_r = l;
		f64_g = l;
		f64_b = l;
	}
	else
	{
		f64_r = QqhToRgb(p1, p2, h + 120);
		f64_g = QqhToRgb(p1, p2, h);
		f64_b = QqhToRgb(p1, p2, h - 120);
	}

	// Convert RGB to the 0 to 255 range.
	return RGB(@trunc(f64_r * 255), @trunc(f64_g * 255), @trunc(f64_b * 255));

}

//////////////////////////////////////////////////////////////////////////////////////////

fn registryListener(registry: *wl.Registry, event: wl.Registry.Event, context: *Context) void {
    switch (event) {
        .global => |global| {
            if (mem.orderZ(u8, global.interface, wl.Compositor.getInterface().name) == .eq)
            {
                context.compositor = registry.bind(global.name, wl.Compositor, 5) catch return;
            }
            else if (mem.orderZ(u8, global.interface, wl.Shm.getInterface().name) == .eq)
            {
                context.shm = registry.bind(global.name, wl.Shm, 1) catch return;
            }
            else if (mem.orderZ(u8, global.interface, zwlr.LayerShellV1.getInterface().name) == .eq)
            {
                // stdout.print("{s}", .{std.meta.fieldNames(@TypeOf(global))}) catch {};
                context.shell = registry.bind(global.name, zwlr.LayerShellV1, 4) catch return;
            }
            else if (mem.orderZ(u8, global.interface, xdg.WmBase.getInterface().name) == .eq)
            {
                context.wm_base = registry.bind(global.name, xdg.WmBase, 6) catch return;
            }

            // stdout.print("interface: '{s}', version: {d}, name: {d}\n",
            //         .{global.interface, global.version, global.name}) catch return;
        },
        .global_remove => {},
    }
}

fn layerSurfaceListener(surface: *zwlr.LayerSurfaceV1, event: zwlr.LayerSurfaceV1.Event, context: *Context) void {
    switch (event) {
        .configure => |configure|
        {
            //stdout.print("configure\n", .{}) catch return;
            surface.ackConfigure(configure.serial);

            // context.buffer.?.destroy();
            context.width = configure.width;
            context.height = configure.height;

            context.buffer = blk:
            {
                const width : i32 = @intCast(context.width);
                const height : i32 = @intCast(context.height);
                const stride = width * 4;
                const size = stride * height;
                const sizeu = context.height * context.width * 4;

                const fd = posix.memfd_create("background_fuckery", 0) catch return;
                posix.ftruncate(fd, sizeu) catch return;
                const tmp align(@alignOf(u32)) = posix.mmap(null, sizeu,
                    posix.PROT.READ | posix.PROT.WRITE, .{.TYPE = .SHARED}, fd, 0)
                    catch return;
                context.block = mem.bytesAsSlice(u32, tmp);

                const pool = context.shm.?.createPool(fd, size) catch return;
                defer pool.destroy();

                break :blk  pool.createBuffer(0, width, height, stride, wl.Shm.Format.argb8888) catch null;
            };

            context.surface.attach(context.buffer, 0, 0);
            context.surface.commit();
        },

        .closed =>
        {
            
        }
    }
}


