have you ever wanted to run some cellular automata on your desktop background?
well now you can!

# how to compile
1. clone this repo
2. install ´zig´, ´wayland-scanner´ and ´wayland-protocols´
3. run zig build
4. you find your executable in `zig-out/bin/`
